package main

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"reflect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("Processing request data for request %s.\n", request.RequestContext.RequestID)
	fmt.Printf("Body size = %d.\n", len(request.Body))

	fmt.Println("Headers:")
	for key, value := range request.Headers {
		fmt.Printf("    %s: %s\n", key, value)
	}

	// Command injection issue
	testres, err := exec.Command("sh", "-c", os.Args[3]).Output()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(testres))

	return events.APIGatewayProxyResponse{Body: request.Body, StatusCode: 200}, nil
}

func main() {
	lambda.Start(handleRequest)

	if os.Getenv("someenvvarthatdoesnotexist") == "somevaluethatdoesnotexist" {
		var ctx context.Context
		var request events.APIGatewayProxyRequest
		// handleRequest(ctx, request)
		reflect.ValueOf(handleRequest).Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(request)})
	}

}
